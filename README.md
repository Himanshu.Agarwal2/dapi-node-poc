## Introduction
DAPI doing POC on Node.js to implement all the functionality presnt in current DAPI services.

## Stack

POC uses the following software stack:

- [Node.js] - evented I/O for the backend systems.
- [TypeScript] - extending JavaScript with types.

## Installation
1. You need to ensure the following are installed:
- [Node.js] v10.15.1

2. Place the following `.npmrc` file in your user directory: 
registry=https://nexus.int.ally.com/nexus/repository/npm-group/
strict-ssl=false
unsafe-perm=true


3. Create Node js application in jboss:

   a. Click File -> New -> Other and type JavaScript in the search text box.

   b. Select JavaScript Project and click Next.

   c. In the Project Name field, add a name for your new project.

   d. Ensure that the rest of the fields, which are set to the default settings, are as required, and then click Finish to create the new project.

   e. If asked to view the JavaScript perspective, click Yes. Your new project is listed in the Project Explorer view.

4. To interactively create a package.json file:

   a. Click File -> New -> Other and then type npm in the search box.

   b. From the search results, click npm Init.

   c. Set the Base directory to your JavaScript project folder in your JBoss Tools workspace.

   d. Optionally, clear the Use default configuration check box to supply non-default values for these fields.

   e. Click Finish to continue with the default values for the package.json file or to continue after changing the default values.

More details are present here : https://tools.jboss.org/documentation/howto/develop_nodejs_apps.html

## Run locally :

1.created main.ts file and and added code for starting the local-server.
2.Right cick on the project and run as "nodejs application".
3. Give the main.ts file in run configuration and click on run.
Alternate way : open Node.js command prompt and go to the project path where main.ts fie present and use below command
				"Node main.ts"
4. Install modules : to install any module use below command where package.json is present :
   npm install {module name}.

