const config = {
  preset: 'ts-jest',
  cacheDirectory: './.cache/jest',
  collectCoverage: true,
  coverageDirectory: 'coverage',
  coverageReporters: ['html', 'lcov', 'json', 'text'],
  testEnvironment: 'node',
  testMatch: ['**/__tests__/**.test.js,', '**/__tests__/**.test.ts'],
  collectCoverageFrom: ['./src/**/*.ts'],
  coveragePathIgnorePatterns: ['./src/models', '__fixtures__'],
  coverageThreshold: {
    global: {
      branches: 95,
      functions: 95,
      lines: 95,
      statements: 95,
    },
  },
  globalSetup: './global-setup.js',
};

module.exports = config;