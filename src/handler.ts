const awsServerlessExpress = require('aws-serverless-express')
const { appMain } = require("../main");

const server = awsServerlessExpress.createServer(appMain);

/**
 * accounts-api lambda handler
 * @param event - lambda event
 * @param context - lambda context
 */
export const handler = (event, context) => awsServerlessExpress.proxy(server, event, context);