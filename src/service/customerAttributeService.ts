
const configDb = require('../dao/customerDBConfig.ts')
const attributeObject = require('../bean/attribute.ts')
const oracledbModule = require('oracledb');
const date = require('date-and-time');
let attributeDataFromDB = [];
//var sequence = require('sequence-generator').sequenceGenerator("collectionId");
function delayTimes(n) {
    return new Promise(function (resolve) {
        setTimeout(resolve, n * 1000);
    });
}

async function customerAttributeService(cif,cupid,attributeName, identifierValue, identifierType) {

    try {
        const pool = await oracledbModule.createPool(configDb);
        const selectQuery = "SELECT * FROM ATTRIBUTE where NAME= '" + attributeName + "' and " + identifierType + " = '" + identifierValue + "'";
        var connection = await pool.getConnection();
    connection.execute(selectQuery, [], function (err, result) {
            if (err) {
                console.error(err.message);
                connection.close();
                return;
            }
            attributeDataFromDB = result.rows;
            connection.close();


        });
        await delayTimes(1);
        console.log("attributeDataFromDB::::::::::::::::::::::::::::::::: ", attributeDataFromDB);
        if (attributeDataFromDB.length !== 0) {

            return new attributeObject.attribute(attributeDataFromDB);
        }
        else {
            connection = await pool.getConnection();
            let current_datetime = new Date()
            const dateModified = date.format(current_datetime, 'DD-MMM-YY');
            const defaulltValue = "0";
            const updateBy = "SYSTEM"
            let maxAttributeIdValue = 1;
            connection.execute("SELECT MAX(ATTRIBUTE_ID) FROM ATTRIBUTE", [], function (err, result) {

                if (err) {
                    console.error(err.message);
                    connection.close();
                    return;
                }
                //console.log("max value is ::::::::::::::::::", result);
                maxAttributeIdValue = result.rows;
                console.log("max value is ::::::::::::::::::", maxAttributeIdValue);
                connection.close();
            });
            await delayTimes(1);
            connection = await pool.getConnection();
            const encryptedAccount = null;
            const expirationDate = null;
            const attributeID = parseInt(maxAttributeIdValue[0]) + 1;
            await connection.execute("INSERT INTO ATTRIBUTE(ATTRIBUTE_ID,CIF,ENCRYPTEDACCOUNT,NAME,VALUE,EXPIRATION_DATE,MODIFIED_BY,LAST_MODIFIED_DATE,CUP_ID) VALUES(" + attributeID +
                ",'" + String(cif) + "','" + String(encryptedAccount) + "','" + String(attributeName) + "','" + String(defaulltValue) +
                "'," + String(expirationDate) + ",'" + String(updateBy) + "','" + String(dateModified) + "','" + String(cupid) + "')", [], { autoCommit: true }, function (err, result) {
                    if (err) {
                        console.error(err.message);
                        connection.close();
                        return;
                    }
                    console.log("after value of sequence ::::::::3::::::", attributeDataFromDB);
                    if (result.rowsAffected > 0) {
                        attributeDataFromDB = new attributeObject.attributeInsert(attributeName, defaulltValue);
                    }


                    connection.close();
                });


        }
        await delayTimes(1);
        return attributeDataFromDB;
    } catch (error) {
        console.log("error in catch", error);
    }
}

module.exports = { customerAttributeService };