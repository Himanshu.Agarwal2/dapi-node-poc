const crypto = require('crypto');
// This is the data we want to encrypt
const data = "my secret data";

const encryptedData = crypto.publicEncrypt(
    {
        key: publicKey,
        padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
        oaepHash: "sha256",
    },
    // We convert the data string to a buffer using `Buffer.from`
    Buffer.from(data)
);

// The encrypted data is in the form of bytes, so we print it in base64 format
// so that it's displayed in a more readable form
console.log("encypted data: ", encryptedData.toString("base64"));

//var hw = encrypt("Some serious stuff")
//console.log(hw)
//console.log(decrypt('eOHEfEtMEK4Y08ERHURATaTjH1wfIVjaiDVQTye3prr/tX69dn6KfoAkW4LSt+VnztDnTbPkqyv2B3y82MiE1AWAoYeHM9l8gBX3BGxHh+VWioNw4kzlyu6ZVqsk5YNHWMpiunnprxzvNAgJaJ5ErNIQejW5o6WygMK2bFifz3kg2Wir/ceSiq5twFF14KkJ+0zvCs9oWpJHQElawp5TgIHbUpbOFCZVj66zbDfMpm9+lEQEBwOX+63Tpz/TYSIk3DrcwX/T/gvJD/COYyFRIi1pOJTUgzuPgkl35jjpNPe+TQRb0rC+M0PWHQ5sOBlQ8fobtMIY9nSqcgniyXeqZw=='))