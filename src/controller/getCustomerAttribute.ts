
const expressGetCustomerAttribute = require('express');
const getCustomerAttributeRouter = expressGetCustomerAttribute.Router();
var validation = require('../bean/headerValidation.ts');
var session = require('express-session');
const getCustomerAttributeMap = require('../enum/customerAttribute.ts');
const getCustomerAttributeService = require('../service/customerAttributeService.ts');

getCustomerAttributeRouter.get('/customers/attributes/:attributeName', (req, res) => {

    var attributeName = req.params.attributeName;
    var cif = req.query.cif;
    console.log("value of cif::::::::::::::::::::::::::::::: ", cif);
    var cupid = req.query.cupid;
    console.log("value of cupid::::::::::::::::::::::::::::::: ", cupid);

    var identifierValue = null;
    var identifierType = null;
    if (typeof cif !== "undefined") {
        identifierType = "CIF";
        identifierValue = cif;
    } else {
        identifierType = "CUP_ID";
        identifierValue = cupid;
        cif = null;
    }



    // assumes bearer token is passed as an authorization header
  //  if (req.headers.authorization) {
        // configure the request to your keycloak server
      //  const options = {
      //      method: 'GET',
       //     url: config.get('url'),
         //   headers: {
                // add the token you received to the userinfo request, sent to keycloak
          //      Authorization: req.headers.authorization,
          //  },
     //   };

        // send a request to the userinfo endpoint on keycloak
      //  request(options, (error, response, body) => {
         //   if (error) throw new Error(error);

            // if the request status isn't "OK", the token is invalid
          //  if (response.statusCode !== 200) {
             //   res.status(401).json({
              //      error: `unauthorized`,
              //  });
         //   }
            // the token is valid pass request onto your next function
          //  else {
               // var validateHeaders = validation.validationErrors(req);
               // console.log(validateHeaders.length)
               // if (validateHeaders.length > 0) {
                //    expressError(validateHeaders, req, res, null);
                   // res.end();
               // }
               // else {

                    //checking if correct attribute name is passed or not
    console.log(getCustomerAttributeMap.customerAttributeMap.get(attributeName));
    if (typeof getCustomerAttributeMap.customerAttributeMap.get(attributeName) === "undefined") {
        res.status(401).json({
            "errors": [{ "errorDescription": "Customer attribute name is invalid" }]
        });
        res.end();
    } else {
        (async () => {
            console.log("coming into async:::::::::::::::::::::::");
            var attribute = await getCustomerAttributeService.customerAttributeService(cif, cupid, attributeName, identifierValue, identifierType);
            console.log("response in controller:::::::::::::::::::::::", attribute);

            res.status(200).json({
                attribute
            });
            res.end();
        })()
    }
                       // if (response instanceof httpErrors.ResponseNotValidError) {
                          //  res.status(503).json({ "errors": response });
                         //   res.end();
                      //  }
                       // console.log("response in controller ::::::: ", response);
                       // res.status(200).json({ "customer": response });
                      //  res.end();
                   // })()
              //  }
          //  }
       // });
  //  } else {
        // there is no token, don't process request further
    //    res.status(401).json({
       //     error: `unauthorized`,
      //  });
  //  }
});

module.exports = getCustomerAttributeRouter