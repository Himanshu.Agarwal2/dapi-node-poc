
const expressMain = require('express');
var appMain = expressMain();
const morgan = require('morgan');
var bodyParser = require('body-parser');
appMain.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
appMain.use(bodyParser.json());
//const config = require("config");
//const ip = config.get('app.ip');
//console.log("ip value::::::::::::::::::::::",ip);
//const oracledb = require('oracledb');
//const dbPool = oracledb.createPool({
 //   user: "OLB_PDEV_APPS",
   // password: "Ap5D30L#Feb",
    //connectString: "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST   = sat1lvddb401-scan.ally.corp)(PORT = 2020))(CONNECT_DATA =(SERVICE_NAME=MBNKD)))",
    //poolMin: 10,
    //poolMax: 50,
    //poolIncrement: 1
//});

appMain.use(bodyParser.urlencoded({
  extended: true
})); //init routes



var getCustomer = require('./controller/getCustomer.ts');

appMain.use(getCustomer);

var getCustomerAttribute = require('./controller/getCustomerAttribute.ts');

appMain.use(getCustomerAttribute);



appMain.listen(8080);
console.log('Node server running on port 8080');

module.exports = appMain;
