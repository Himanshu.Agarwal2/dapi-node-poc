const express = require('express');
const router = express.Router();
const config = require('./dbConfig.ts')
var oracledb = require('oracledb');
router.get('/dblink', (req, res) => {
    async function runTest() {
    const startTime = Date.now();
    for (let x = 0; x < 100; x++) {
        const connection = await oracledb.getConnection(config);
        // console.log(connection);
        connection.execute("SELECT * FROM BILLPAY_ENROLLED where CUSTOMER_IDENTIFIER='150062'", [], function (err, result) {
            if (err) {
                console.error(err.message);
                connection.close();
                return;
            }
            console.log(result.metaData);
            //  console.log(result.rows);
            connection.close();
        });
            };
            console.log('total time : ', Date.now() - startTime);
    }
    runTest();
    res.end();
});

module.exports = router