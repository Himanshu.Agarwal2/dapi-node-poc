/**
 * Class represents Account http error
 */

class ResponseNotValidError{

    constructor(errorCode, errorDescription, errorField) {
		// @ts-ignore
        this.errorCode = errorCode;
		// @ts-ignore
        this.errorDescription = errorDescription;
		// @ts-ignore
        this.errorField = errorField;
    }
}

/**
 * Class represents Account http unauthorized error
 */
class UnauthorizedError {

    constructor(errorCode, errorDescription, errorField) {
		// @ts-ignore
        this.errorCode = errorCode;
		// @ts-ignore
        this.errorDescription = errorDescription;
		// @ts-ignore
        this.errorField = errorField;
    }
}

class BadRequestError {

    constructor(errorCode, errorDescription, errorField) {
		// @ts-ignore
        this.errorCode = errorCode;
		// @ts-ignore
        this.errorDescription = errorDescription;
		// @ts-ignore
        this.errorField = errorField;
    }
}

module.exports = { ResponseNotValidError, UnauthorizedError, BadRequestError };