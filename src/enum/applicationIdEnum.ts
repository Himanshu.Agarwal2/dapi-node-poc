const applicationIdMap = new Map();
applicationIdMap.set('ALLYUSBOLB', 'ALLYUSBOLB');
applicationIdMap.set('ALLYUSBBOT', 'ALLYUSBBOT');
applicationIdMap.set('ALLYUSBDAO', 'ALLYUSBDAO');
applicationIdMap.set('ALLYUSBIDP', 'ALLYUSBIDP');
applicationIdMap.set('ALLYUSBMBWEB', 'ALLYUSBMBWEB');
applicationIdMap.set('ALLYUSBSFS', 'ALLYUSBSFS');
applicationIdMap.set('ALLYUSBSFA', 'ALLYUSBSFA');
applicationIdMap.set('ALLYUSBMBAPP', 'ALLYUSBMBAPP');
applicationIdMap.set('ALLYUSBIMBAPP', 'ALLYUSBIMBAPP');
applicationIdMap.set('ALLYUSBATBAPP', 'ALLYUSBATBAPP');
applicationIdMap.set('ALLYUSBKTBAPP', 'ALLYUSBKTBAPP');

 // Trade King Application Id's
applicationIdMap.set('ALLYUSBOLA', 'ALLYUSBOLA');

applicationIdMap.set('ALLYUSBINV', 'ALLYUSBINV');

const applicationNameMap = new Map();
applicationNameMap.set('AOB', 'AOB');


module.exports = { applicationIdMap, applicationNameMap }


