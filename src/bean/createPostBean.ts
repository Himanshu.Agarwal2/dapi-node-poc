
const { check, validationResult } = require('express-validator');
const expressCreatePost = require('express');
const appCreatePost = expressCreatePost();
const ValidationErrors = (req) => {
   // app.use(expressValidator.check());
   // app.use(expressValidator.validationResult());
   check(req.body.username).isEmail();
   // console.log(isEmail);
    const errors = validationResult(req);
    return errors;
};
module.exports = { validationErrors }