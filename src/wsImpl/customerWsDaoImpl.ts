const httpErrors = require("../model/httpError.ts");
function delay(n) {
    return new Promise(function (resolve) {
        setTimeout(resolve, n * 1000);
    });
}

async function customerwsImpl(cif) {
    var numericCif = Number(cif);
    console.log(numericCif);
    console.log(cif);
    var returnResponse = "";
    const axios = require("axios");
    const crypto = require('crypto');
    const {BigNumber} = require('bignumber');
    var forge = require('node-forge');
    var fs = require('fs');
    const soap = require('soap');
    var xmlenc = require('xml-encryption');
    const exec = require('child_process').exec;

    const rqId = Math.random().toString(36).slice(2);

    const clientDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString().split('.')[0];

    const custLangPref = 'en_US'
    var secHeader;

    try {
        const childPorcess = await exec('java -jar ../dapi-node-poc/fis-security-header.jar', function (err, stdout, stderr) {


            if (err) {
                console.log("error after child process:::::::",err)
            }
            secHeader = stdout;
           // console.log("security header ::::::::::::::::::::::::: ", secHeader);
        });
    } catch (err) { console.log("security header in catch::::::::::::::::::::::::: ", secHeader); }
    const buildFISRequest = () =>
        `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">\
    ${secHeader}<soapenv:Body>\
       <IFX xmlns:com.fnf="http://www.fnf.com/xes" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.ifxforum.org/IFX_150" xmlns:com.fnf.custinq_V2_1="http://www.fnf.com/xes/services/cust/custinq/v2_1">\
          <SignonRq Id="ID000000">\
             <SessKey/>\
             <ClientDt>${clientDate}</ClientDt>\
             <CustLangPref>${custLangPref}</CustLangPref>\
             <ClientApp>\
                <Org>com.Fidelity</Org>\
                <Name>Fidelity Bank</Name>\
                <Version>1.0</Version>\
             </ClientApp>\
             <SuppressEcho>0</SuppressEcho>\
          </SignonRq>\
          <BaseSvcRq Id="ID000001">\
             <RqUID>${rqId}</RqUID>\
             <SPName>com.fnf.xes.PRF</SPName>\
             <com.fnf.custinq_V2_1:CustInqRq>\
                <RqUID>${rqId}</RqUID>\
                <CustId>\
                   <SPName>com.fnf.xes.PRF</SPName>\
                   <CustPermId>${cif}</CustPermId>\
                </CustId>\
             </com.fnf.custinq_V2_1:CustInqRq>\
          </BaseSvcRq>\
       </IFX>\
    </soapenv:Body>\
 </soapenv:Envelope>`;

    const soapHeaders = {
        headers: {
            'Content-Type': 'text/xml',
        },
    };


    await delay(2);

    const soapRequest = buildFISRequest();
    try {
        console.log("coming into try block::::::");
        const { data } = await axios.post("https://dapi-vendor-proxy.ocp-dev.int.ally.com/axis/services/IFXService2", soapRequest, soapHeaders);
        console.log("data::::::", data);
        returnResponse = data;
        
    } catch (err) {
        console.log("coming into catch block::::::");
        return new httpErrors.ResponseNotValidError("USB_SYS_058", 'Sorry, our system is temporarily unavailable. Please try again later or call us 24/7 for help.', '')
    }

        return returnResponse;
    };




module.exports = { customerwsImpl }