class Customer {

    constructor(birthDate, etinStatusCodePvtEncrypt, statementDeliveryPreference, mothersMaidenName, backupWithholdingFlagPvtEncrypt, backupWithholdingReasonCode, ssnPvtblock, accountOpeningDate, segmentCode, restrictInd, passPhraseQuestion, passPhraseAnswer) {
        // @ts-ignore
        this.age = calculateAge(birthDate);
        // @ts-ignore
        this.etinStatusCodePvtEncrypt = etinStatusCodePvtEncrypt;
        // @ts-ignore
        this.statementDeliveryPreference = statementDeliveryPreference;
        // @ts-ignore
        this.mothersMaidenName = mothersMaidenName;
        // @ts-ignore
        this.backupWithholdingFlagPvtEncrypt = backupWithholdingFlagPvtEncrypt;
        // @ts-ignore
        this.backupWithholdingReasonCode = backupWithholdingReasonCode;
        // @ts-ignore
        this.ssnPvtblock = ssnPvtblock;
        // @ts-ignore
        this.accountOpeningDate = new Date(accountOpeningDate);
        // @ts-ignore
        this.segmentCode = segmentCode;
        // @ts-ignore
        this.restrictInd = restrictInd;
        // @ts-ignore
        this.passPhraseQuestion = passPhraseQuestion;
        // @ts-ignore
        this.passPhraseAnswer = passPhraseAnswer;

    }
}

function calculateAge(birthDate){
    var today = new Date();
    var parseBirthDate = new Date(birthDate);
    console.log(today);
    console.log(parseBirthDate);

    console.log(today.getMonth());
    console.log(parseBirthDate.getMonth());
    //calculation of months
        var yearGap = today.getFullYear() - parseBirthDate.getFullYear() + ((today.getMonth() - parseBirthDate.getMonth()) / 10);

        return yearGap;
}


module.exports = { Customer };