const customerWSResponse1 = '<?xml version="1.0" encoding="UTF-8"?>\
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">\
	<soapenv:Body>\
		<IFX xmlns="http://www.ifxforum.org/IFX_150">\
			<SignonRs>\
				<ServerDt>2021-11-18T00:00:00.000000-00:00</ServerDt>\
				<Language>en_US</Language>\
			</SignonRs>\
			<BaseSvcRs>\
				<RqUID>w5qa3v9afaj</RqUID>\
				<com.fnf.custinq.v2_1:CustInqRs\
					xmlns:com.fnf.custinq.v2_1="http://www.fnf.com/xes/services/cust/custinq/v2_1">\
					<Status>\
						<StatusCode>0</StatusCode>\
						<Severity>Info</Severity>\
						<StatusDesc>Success</StatusDesc>\
					</Status>\
					<RqUID>w5qa3v9afaj</RqUID>\
					<com.fnf.custinq.v2_1:CustRec>\
						<CustId>\
							<SPName>com.fnf.xes.PRF</SPName>\
							<CustPermId>9319860</CustPermId>\
						</CustId>\
						<CustInfo>\
							<PersonInfo>\
								<NameAddrType />\
								<PersonName>\
									<LastName>raju</LastName>\
									<FirstName>rama</FirstName>\
									<MiddleName>H</MiddleName>\
									<TitlePrefix>Mr.</TitlePrefix>\
									<NameSuffix>Jr.</NameSuffix>\
								</PersonName>\
								<BirthDt>1960-04-05</BirthDt>\
								<MotherMaidenName>MOM</MotherMaidenName>\
								<EmploymentHistory>\
									<Occupation>Unavailable</Occupation>\
									<Income>\
										<Amt>0</Amt>\
										<CurCode>USD</CurCode>\
									</Income>\
								</EmploymentHistory>\
								<Dependents>0</Dependents>\
							</PersonInfo>\
						</CustInfo>\
						<com.fnf:CustAdditionalInfo\
							xmlns:com.fnf="http://www.fnf.com/xes">\
							<com.fnf:PersonAdditionalInfo>\
								<com.fnf:ResidenceInfo>\
									<com.fnf:ForeignStatusFormReqiredInd>0\
									</com.fnf:ForeignStatusFormReqiredInd>\
								</com.fnf:ResidenceInfo>\
								<com.fnf:FamilyInfo>\
									<com.fnf:SpouseInfo />\
									<com.fnf:FamilyFinancialInfo>\
										<com.fnf:IncomeRange>\
											<LowCurAmt>\
												<Amt>0</Amt>\
												<CurCode>USD</CurCode>\
											</LowCurAmt>\
											<HighCurAmt>\
												<Amt>0</Amt>\
												<CurCode>USD</CurCode>\
											</HighCurAmt>\
										</com.fnf:IncomeRange>\
									</com.fnf:FamilyFinancialInfo>\
								</com.fnf:FamilyInfo>\
								<com.fnf:DependentsInfo>\
									<com.fnf:RetirementAcctHolderInd>1\
									</com.fnf:RetirementAcctHolderInd>\
								</com.fnf:DependentsInfo>\
								<com.fnf:EducationInfo />\
							</com.fnf:PersonAdditionalInfo>\
							<com.fnf:CustBasicInfo>\
								<CustType>Personal</CustType>\
								<com.fnf:RelCd>0</com.fnf:RelCd>\
								<com.fnf:CustSubType>0</com.fnf:CustSubType>\
								<com.fnf:CustRelType>Rate Code Zero</com.fnf:CustRelType>\
								<com.fnf:CostCenter>111001</com.fnf:CostCenter>\
								<SecretList>\
									<SecretId />\
									<CryptType />\
									<Secret>MOM</Secret>\
								</SecretList>\
								<com.fnf:DirectMktgMail>0</com.fnf:DirectMktgMail>\
								<com.fnf:DirectMktgEmail>0</com.fnf:DirectMktgEmail>\
								<com.fnf:DirectMktgOutboundPhone>0\
								</com.fnf:DirectMktgOutboundPhone>\
							</com.fnf:CustBasicInfo>\
							<com.fnf:CustFinancialInfo />\
							<com.fnf:CustActivityInfo>\
								<com.fnf:CustLastActivityDt>2021-11-13\
								</com.fnf:CustLastActivityDt>\
								<com.fnf:DoNotDeleteCustInd>0</com.fnf:DoNotDeleteCustInd>\
								<com.fnf:RestrictInd>0</com.fnf:RestrictInd>\
							</com.fnf:CustActivityInfo>\
							<com.fnf:CustRepresentativeInfo />\
							<com.fnf:CustShareHoldingInfo />\
							<com.fnf:CustCommunicationInfo>\
								<com.fnf:CustLastContactDt>2021-11-13\
								</com.fnf:CustLastContactDt>\
							</com.fnf:CustCommunicationInfo>\
							<com.fnf:CustAcctRelatedInfo>\
								<com.fnf:BackupWithholding>FALSE</com.fnf:BackupWithholding>\
								<BankInfo>\
									<BranchId>3</BranchId>\
								</BankInfo>\
								<com.fnf:StmtInfo>\
									<com.fnf:StmtGroup>98</com.fnf:StmtGroup>\
									<com.fnf:Frequency>\
										<Freq>Quarterly</Freq>\
										<com.fnf:FreqAdditionalInfo>\
											<com.fnf:DayOfMonth>3</com.fnf:DayOfMonth>\
										</com.fnf:FreqAdditionalInfo>\
										<com.fnf:BusinessDayOption>A</com.fnf:BusinessDayOption>\
									</com.fnf:Frequency>\
									<com.fnf:StmtDeliveryPref>Tickler</com.fnf:StmtDeliveryPref>\
								</com.fnf:StmtInfo>\
								<com.fnf:BackupWithholdingReason>Voluntary\
								</com.fnf:BackupWithholdingReason>\
							</com.fnf:CustAcctRelatedInfo>\
							<com.fnf:IdInfo>\
								<com.fnf:CompositeTINInfo>\
									<TINInfo>\
										<TINType>SSN</TINType>\
										<TaxId>520-78-9621</TaxId>\
									</TINInfo>\
									<com.fnf:W8W9CertInfo>\
										<com.fnf:CertStatusCode>Certified</com.fnf:CertStatusCode>\
										<com.fnf:CertMethod>eTINCertified</com.fnf:CertMethod>\
									</com.fnf:W8W9CertInfo>\
								</com.fnf:CompositeTINInfo>\
								<Country />\
							</com.fnf:IdInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1>Diamon street</Addr1>\
									<Addr2>kio</Addr2>\
									<City>LA</City>\
									<StateProv>CA</StateProv>\
									<PostalCode>90018</PostalCode>\
									<Country>US</Country>\
									<AddrType>Residence</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1>ARC sTREET</Addr1>\
									<Addr2>xyz</Addr2>\
									<City>LA</City>\
									<StateProv>CA</StateProv>\
									<PostalCode>90001</PostalCode>\
									<Country>US</Country>\
									<AddrType>Mailing</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Seasonal</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Previous</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:CustCommInfo>\
								<PhoneNum>\
									<PhoneType>EvePhone</PhoneType>\
									<Phone>884-411-2233</Phone>\
								</PhoneNum>\
							</com.fnf:CustCommInfo>\
							<com.fnf:CustCommInfo>\
								<PhoneNum>\
									<PhoneType>DayPhone</PhoneType>\
									<Phone>903-375-6747+40</Phone>\
								</PhoneNum>\
							</com.fnf:CustCommInfo>\
							<com.fnf:CustCommInfo>\
								<PhoneNum>\
									<PhoneType>Mobile</PhoneType>\
									<Phone>880-911-4477</Phone>\
								</PhoneNum>\
							</com.fnf:CustCommInfo>\
							<com.fnf:CustCommInfo>\
								<com.fnf:EmailAddrType>DayEmail</com.fnf:EmailAddrType>\
								<EmailAddr>sandhya.lingwal@ally.com</EmailAddr>\
							</com.fnf:CustCommInfo>\
							<com.fnf:CustEmpInfo>\
								<EmploymentHistory>\
									<OrgInfo>\
										<IndustId>\
											<Org />\
											<IndustNum />\
										</IndustId>\
										<LegalName />\
										<CompositeContactInfo>\
											<ContactInfoType />\
											<ContactInfo>\
												<PhoneNum>\
													<PhoneType />\
													<Phone />\
												</PhoneNum>\
											</ContactInfo>\
										</CompositeContactInfo>\
									</OrgInfo>\
									<Occupation>Unavailable</Occupation>\
								</EmploymentHistory>\
							</com.fnf:CustEmpInfo>\
							<com.fnf:CustLendLimitsInfo>\
								<ExtAcctBal>\
									<ExtBalType>Uncollected</ExtBalType>\
									<CurAmt>\
										<Amt>-4359.54</Amt>\
										<CurCode>USD</CurCode>\
									</CurAmt>\
									<EffDt />\
								</ExtAcctBal>\
								<ExtAcctBal>\
									<ExtBalType>Used</ExtBalType>\
									<CurAmt>\
										<Amt>4359.54</Amt>\
										<CurCode>USD</CurCode>\
									</CurAmt>\
									<EffDt />\
								</ExtAcctBal>\
							</com.fnf:CustLendLimitsInfo>\
						</com.fnf:CustAdditionalInfo>\
						<com.fnf:LogInfo xmlns:com.fnf="http://www.fnf.com/xes">\
							<com.fnf:EmployeeId>PD8903</com.fnf:EmployeeId>\
							<com.fnf:CustAcctOpenDt>2019-11-08</com.fnf:CustAcctOpenDt>\
							<com.fnf:CustAcctOpenTime>00:44:58</com.fnf:CustAcctOpenTime>\
						</com.fnf:LogInfo>\
					</com.fnf.custinq.v2_1:CustRec>\
				</com.fnf.custinq.v2_1:CustInqRs>\
			</BaseSvcRs>\
		</IFX>\
	</soapenv:Body>\
</soapenv:Envelope>';

const customerWSResponse2 = '<?xml version="1.0" encoding="UTF-8"?>\
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">\
	<soapenv:Body>\
		<IFX xmlns="http://www.ifxforum.org/IFX_150">\
			<SignonRs>\
				<ServerDt>2021-11-18T00:00:00.000000-00:00</ServerDt>\
				<Language>en_US</Language>\
			</SignonRs>\
			<BaseSvcRs>\
				<RqUID>w5qa3v9afaj</RqUID>\
				<com.fnf.custinq.v2_1:CustInqRs\
					xmlns:com.fnf.custinq.v2_1="http://www.fnf.com/xes/services/cust/custinq/v2_1">\
					<Status>\
						<StatusCode>0</StatusCode>\
						<Severity>Info</Severity>\
						<StatusDesc>Success</StatusDesc>\
					</Status>\
					<RqUID>w5qa3v9afaj</RqUID>\
					<com.fnf.custinq.v2_1:CustRec>\
						<CustId>\
							<SPName>com.fnf.xes.PRF</SPName>\
							<CustPermId>9319860</CustPermId>\
						</CustId>\
						<CustInfo>\
							<PersonInfo>\
								<NameAddrType />\
								<PersonName>\
									<LastName>raju</LastName>\
									<FirstName>rama</FirstName>\
									<MiddleName>H</MiddleName>\
									<TitlePrefix>Mr.</TitlePrefix>\
									<NameSuffix>Jr.</NameSuffix>\
								</PersonName>\
								<BirthDt>1960-04-05</BirthDt>\
								<MotherMaidenName>MOM</MotherMaidenName>\
								<EmploymentHistory>\
									<Occupation>Unavailable</Occupation>\
									<Income>\
										<Amt>0</Amt>\
										<CurCode>USD</CurCode>\
									</Income>\
								</EmploymentHistory>\
								<Dependents>0</Dependents>\
							</PersonInfo>\
						</CustInfo>\
						<com.fnf:CustAdditionalInfo\
							xmlns:com.fnf="http://www.fnf.com/xes">\
							<com.fnf:PersonAdditionalInfo>\
								<com.fnf:ResidenceInfo>\
									<com.fnf:ForeignStatusFormReqiredInd>0\
									</com.fnf:ForeignStatusFormReqiredInd>\
								</com.fnf:ResidenceInfo>\
								<com.fnf:FamilyInfo>\
									<com.fnf:SpouseInfo />\
									<com.fnf:FamilyFinancialInfo>\
										<com.fnf:IncomeRange>\
											<LowCurAmt>\
												<Amt>0</Amt>\
												<CurCode>USD</CurCode>\
											</LowCurAmt>\
											<HighCurAmt>\
												<Amt>0</Amt>\
												<CurCode>USD</CurCode>\
											</HighCurAmt>\
										</com.fnf:IncomeRange>\
									</com.fnf:FamilyFinancialInfo>\
								</com.fnf:FamilyInfo>\
								<com.fnf:DependentsInfo>\
									<com.fnf:RetirementAcctHolderInd>1\
									</com.fnf:RetirementAcctHolderInd>\
								</com.fnf:DependentsInfo>\
								<com.fnf:EducationInfo />\
							</com.fnf:PersonAdditionalInfo>\
							<com.fnf:CustBasicInfo>\
								<CustType>Personal</CustType>\
								<com.fnf:RelCd>0</com.fnf:RelCd>\
								<com.fnf:CustSubType>0</com.fnf:CustSubType>\
								<com.fnf:CustRelType>Rate Code Zero</com.fnf:CustRelType>\
								<com.fnf:CostCenter>111001</com.fnf:CostCenter>\
								<SecretList>\
									<SecretId />\
									<CryptType />\
									<Secret>MOM</Secret>\
								</SecretList>\
								<com.fnf:DirectMktgMail>0</com.fnf:DirectMktgMail>\
								<com.fnf:DirectMktgEmail>0</com.fnf:DirectMktgEmail>\
								<com.fnf:DirectMktgOutboundPhone>0\
								</com.fnf:DirectMktgOutboundPhone>\
							</com.fnf:CustBasicInfo>\
							<com.fnf:CustFinancialInfo />\
							<com.fnf:CustActivityInfo>\
								<com.fnf:CustLastActivityDt>2021-11-13\
								</com.fnf:CustLastActivityDt>\
								<com.fnf:DoNotDeleteCustInd>0</com.fnf:DoNotDeleteCustInd>\
								<com.fnf:RestrictInd>0</com.fnf:RestrictInd>\
							</com.fnf:CustActivityInfo>\
							<com.fnf:CustRepresentativeInfo />\
							<com.fnf:CustShareHoldingInfo />\
							<com.fnf:CustCommunicationInfo>\
								<com.fnf:CustLastContactDt>2021-11-13\
								</com.fnf:CustLastContactDt>\
							</com.fnf:CustCommunicationInfo>\
							<com.fnf:CustAcctRelatedInfo>\
								<com.fnf:BackupWithholding>FALSE</com.fnf:BackupWithholding>\
								<BankInfo>\
									<BranchId>3</BranchId>\
								</BankInfo>\
								<com.fnf:StmtInfo>\
									<com.fnf:StmtGroup>98</com.fnf:StmtGroup>\
									<com.fnf:Frequency>\
										<Freq>Quarterly</Freq>\
										<com.fnf:FreqAdditionalInfo>\
											<com.fnf:DayOfMonth>3</com.fnf:DayOfMonth>\
										</com.fnf:FreqAdditionalInfo>\
										<com.fnf:BusinessDayOption>A</com.fnf:BusinessDayOption>\
									</com.fnf:Frequency>\
									<com.fnf:StmtDeliveryPref>Print</com.fnf:StmtDeliveryPref>\
								</com.fnf:StmtInfo>\
								<com.fnf:BackupWithholdingReason>Voluntary\
								</com.fnf:BackupWithholdingReason>\
							</com.fnf:CustAcctRelatedInfo>\
							<com.fnf:IdInfo>\
								<com.fnf:CompositeTINInfo>\
									<TINInfo>\
										<TINType>SSN</TINType>\
										<TaxId>520-78-9621</TaxId>\
									</TINInfo>\
									<com.fnf:W8W9CertInfo>\
										<com.fnf:CertStatusCode>Certified</com.fnf:CertStatusCode>\
										<com.fnf:CertMethod>eTINCertified</com.fnf:CertMethod>\
									</com.fnf:W8W9CertInfo>\
								</com.fnf:CompositeTINInfo>\
								<Country />\
							</com.fnf:IdInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Retirement</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1>Diamon street</Addr1>\
									<Addr2>kio</Addr2>\
									<City>LA</City>\
									<StateProv>CA</StateProv>\
									<PostalCode>90018</PostalCode>\
									<Country>US</Country>\
									<AddrType>Residence</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1>ARC sTREET</Addr1>\
									<Addr2>xyz</Addr2>\
									<City>LA</City>\
									<StateProv>CA</StateProv>\
									<PostalCode>90001</PostalCode>\
									<Country>US</Country>\
									<AddrType>Mailing</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Seasonal</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:AddrInfo>\
								<PostAddr>\
									<Addr1 />\
									<City />\
									<StateProv />\
									<PostalCode />\
									<Country />\
									<AddrType>Previous</AddrType>\
								</PostAddr>\
								<com.fnf:AdditionalAddrInfo />\
							</com.fnf:AddrInfo>\
							<com.fnf:CustCommInfo>\
								<PhoneNum>\
									<PhoneType>EvePhone</PhoneType>\
									<Phone>884-411-2233</Phone>\
								</PhoneNum>\
							</com.fnf:CustCommInfo>\
							<com.fnf:CustCommInfo>\
								<PhoneNum>\
									<PhoneType>DayPhone</PhoneType>\
									<Phone>903-375-6747+40</Phone>\
								</PhoneNum>\
							</com.fnf:CustCommInfo>\
							<com.fnf:CustCommInfo>\
								<PhoneNum>\
									<PhoneType>Mobile</PhoneType>\
									<Phone>880-911-4477</Phone>\
								</PhoneNum>\
							</com.fnf:CustCommInfo>\
							<com.fnf:CustCommInfo>\
								<com.fnf:EmailAddrType>DayEmail</com.fnf:EmailAddrType>\
								<EmailAddr>sandhya.lingwal@ally.com</EmailAddr>\
							</com.fnf:CustCommInfo>\
							<com.fnf:CustEmpInfo>\
								<EmploymentHistory>\
									<OrgInfo>\
										<IndustId>\
											<Org />\
											<IndustNum />\
										</IndustId>\
										<LegalName />\
										<CompositeContactInfo>\
											<ContactInfoType />\
											<ContactInfo>\
												<PhoneNum>\
													<PhoneType />\
													<Phone />\
												</PhoneNum>\
											</ContactInfo>\
										</CompositeContactInfo>\
									</OrgInfo>\
									<Occupation>Unavailable</Occupation>\
								</EmploymentHistory>\
							</com.fnf:CustEmpInfo>\
							<com.fnf:CustLendLimitsInfo>\
								<ExtAcctBal>\
									<ExtBalType>Uncollected</ExtBalType>\
									<CurAmt>\
										<Amt>-4359.54</Amt>\
										<CurCode>USD</CurCode>\
									</CurAmt>\
									<EffDt />\
								</ExtAcctBal>\
								<ExtAcctBal>\
									<ExtBalType>Used</ExtBalType>\
									<CurAmt>\
										<Amt>4359.54</Amt>\
										<CurCode>USD</CurCode>\
									</CurAmt>\
									<EffDt />\
								</ExtAcctBal>\
							</com.fnf:CustLendLimitsInfo>\
						</com.fnf:CustAdditionalInfo>\
						<com.fnf:LogInfo xmlns:com.fnf="http://www.fnf.com/xes">\
							<com.fnf:EmployeeId>PD8903</com.fnf:EmployeeId>\
							<com.fnf:CustAcctOpenDt>2019-11-08</com.fnf:CustAcctOpenDt>\
							<com.fnf:CustAcctOpenTime>00:44:58</com.fnf:CustAcctOpenTime>\
						</com.fnf:LogInfo>\
					</com.fnf.custinq.v2_1:CustRec>\
				</com.fnf.custinq.v2_1:CustInqRs>\
			</BaseSvcRs>\
		</IFX>\
	</soapenv:Body>\
</soapenv:Envelope>';

module.exports = {customerWSResponse1,customerWSResponse2};